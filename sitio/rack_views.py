# -*- coding: utf_8 -*-
# -*- encoding: utf-8 -*- 
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from sitio.models import *
from django.forms import ModelForm
import simplejson
import types
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, permission_required
from BeautifulSoup import BeautifulSoup
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.forms.models import model_to_dict

@login_required(login_url="/")
def rack_form(request):
    context = RequestContext(request)
    edificios = Edificio.objects.all()
    racks = Lugar.objects.values('num_rack','edificio').distinct()
    return render_to_response("rack_form.html",
                              {'racks':racks,
                               'edificios':edificios}
                              ,context)

@login_required(login_url="/")
def items_in_rack(request,idrack=None):
    if idrack:
        h = Hardware.objects.filter(lugar__num_rack=idrack).exclude(tipo_hdwre='Virtual')
        h = [{'pk':x.pk,
              'nombre':x.nombre,
              'unidades':x.lugar_set.all()[0].unidades,
              'fila':x.lugar_set.all()[0].fila_rack,
              'formato':x.lugar_set.all()[0].formato_nombre,
              'enclousure':x.lugar_set.all()[0].no_enclousure,
              'tipo':x.tipo_hdwre,
              'p_enclousure':x.lugar_set.all()[0].posicion_enclousure} for x in h]
        h = simplejson.dumps(h)
        return HttpResponse(h)
    else:
        return HttpResponse('FAILD DATA ')
        
