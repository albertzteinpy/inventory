from django.conf.urls.defaults import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = patterns('sitio.views',
        url(r'^$','index',name='index'),
        url(r'^welcome$','welcome'),
        url(r'^login$','loginusr',name='loginusr'),
        url(r'^logoff$','logoff',name='logoff'),
    )

urlpatterns += patterns('sitio.inventario_views',
        url(r'^addform$','addform',name='addform'),
        url(r'^addhardware$','addhardware',name='addhardware'),
        url(r'^modelos$','modelosXmarca',name='modelosXmarca'),
        url(r'^responsables$','responsables',name='responsables'),
        url(r'^searching$','search_engine',name='search_engine'),
        url(r'^searching/(?P<items_per_page>\d+)/$','search_engine',name='search_engine'),
        url(r'^migrate$','migration',name='migration'),
        url(r'^details$','details',name='details'),
        url(r'^ending/(?P<idu>\d+)/$','ending',name='ending'),
        url(r'^autofill$','autofill',name='autofill'),
        url(r'^deleting_h/(?P<idu>\d+)/$','deleting_h',name='deleting_h'),
        url(r'^log/(?P<idu>\d+)/$','loggl',name='loggl'),
        url(r'^reporter$','reporter',name='reporter'),
    )
urlpatterns += patterns('sitio.user_views',
    url(r'^users/$','index',name='index'),
    url(r'^users/(?P<idu>\d+)/$','index',name='index'),
    url(r'^userform/$','userform',name='userform'),
    url(r'^userform/(?P<idu>\d+)/$','userform',name='userform'),
    url(r'^deluser/(?P<idu>\d+)/$','deluser',name='deluser'),
    url(r'^addinguser$','addinguser',name='addinguser'),
    )

urlpatterns += patterns('sitio.rack_views',
    url(r'^rack_form/$','rack_form',name='rack_form'),
    url(r'^rack_form/(?P<idu>\d+)/$','rack_form',name='rack_form'),
    url(r'^items_in_rack/(?P<idrack>\d+)/$','items_in_rack',name='items_in_rack'),
    )

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
