# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import get_object_or_404, render_to_response
from django.shortcuts import redirect
from django.template.context import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required, permission_required
import simplejson
from sitio.auth import AuthManager
from sitio.models import * 
from django.db import models
from django.db.models import Q


def index(request):
    context = RequestContext(request)
    user = request.user
    if user.is_active:
        return redirect('/welcome')
    return render_to_response("inicio.html",
                              {'usr':user},
                              context)

def logoff(request):
    logout(request)
    return redirect('/')

@login_required(login_url="/")
def welcome(request):
    fieldsch = {}
    modelos = (Hardware,Network,Features,Responsable_garantia,Software)
    compuestos = {}
    for m in modelos:
        campos = m._meta.fields
        for c in campos:
            if str(c.get_internal_type())=='CharField' and c.name!='antivirus':
                if str(m.__name__).lower()!='hardware':
                    fieldsch[c.name]={'model':str(m.__name__).lower(),
                                  'field':str(c.name),
                                  'verbose_name':str(c.verbose_name)        
                    }
                else:
                    fieldsch[c.name]={'model':'',
                                      'field':str(c.name),
                                      'verbose_name':str(c.verbose_name) 
                    }
    context = RequestContext(request)
    user = request.user
    marca = Marca.objects.all()
    grupos = [(x.name) for x in request.user.groups.all()]

    args = {}

    relationist = {'linux':{'software__plataforma__icontains':'linux'},
                   'as400':{'tipo_hdwre__icontains':'as400'},
                   'almacenamiento':{'tipo_hdwre__icontains':'vtl'},
                   'windows':{'software__plataforma__icontains':'windows'},
                   'admin':'',
                    }

    total = Hardware.objects.all()

    virtual = len(total.filter(tipo_hdwre__icontains='virtual'))
    fisico = len(total.filter(tipo_hdwre__icontains='fisico'))
    otros = len(total.filter(~Q(tipo_hdwre__icontains='fisico') & ~Q(tipo_hdwre__icontains='virtual') ))
    how_often = len(Hardware.objects.filter(**args))

    states = {'virtual':virtual,'total':len(total),'fisico':fisico,'otros':otros}

    FECHAS = (
            ('fecha_registro',u'Fecha de registro')
            ,('hardware__fecha_llegada',u'Fecha llegada'),
            ('fecha_instalacion',u'Fecha Instalación'),
            ('fecha_apagado','Fecha apagado'),
            ('responsable_garantia__fecha_inicio',u'Fecha inicio de garantía'),
            ('responsable_garantia__fecha_fin',u'Fecha fin de garantina'),
            )
    
    return render_to_response("welcome.html",
                              {'usr':user,
                               'marca':marca,
                               'gp':grupos,
                               'how_often':how_often,
                               'fechas':FECHAS,
                               'reports':states,
                                'fields':fieldsch},
                              context)

'''
    WEBSERVICE PARA AUTENTICAR AL USUARIO
    RETORNA UN JSON CON LA RESPUESTA
'''
def loginusr(request):
    usrchek = AuthManager(request)
    check = usrchek.is_pathient()
    
    if check:
        if check.is_active:
            login(request,check)
            response = {'ok':True}
        else:
            response = {'ok':'NOT ACTIVE'}
    else:
        response= {'ok':False}
    response = simplejson.dumps(response)
    return HttpResponse(response)

    context = RequestContext(request)
    user = request.user
    return render_to_response("inicio.html",
                              {'usr':user},
                              context)
