# -*- coding: utf_8 -*-
# -*- encoding: utf-8 -*- 
# -*- coding: utf_8 -*-

from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from sitio.models import *
from django.forms import ModelForm
import simplejson
import types
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, permission_required
from BeautifulSoup import BeautifulSoup
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.forms.models import model_to_dict
import urllib
import urllib2
from django.conf import settings
from django.db.models import Q

def engine_add(hardwareid,response):
    info = {
        'nombre':'',
        'ambiente':'',
        'ip':'',
        'ip_ilo':'',
        'plataforma':'',
        'version':'',
        'release':'',
        'version':'',
        'marca':'',
        'modelo':''
    }
    
    hd = Hardware.objects.get(pk=hardwareid)
    hh,hf = SearchEngine.objects.get_or_create(hardware=hd)
    if hh.texto:
        info_load = simplejson.loads(str(hh.texto))
    else:
        info_load = info
        
    
    intersector = set(info_load) & set(response)

    for x in intersector:
        info_load[x] = response[x]

    info_txt = simplejson.dumps(info_load,True)
    
    hh.texto = info_txt
    hh.save()
    return hh

    
def log_usuarios(request):
    h = Hardware.objects.get(pk=request.POST['id_hardware'])    
    hdict = model_to_dict(h)
    ''' software'''
    models_list = [h.software_set.all()[0],
                   h.lugar_set.all()[0],
                   h.network_set.all()[0],
                   h.responsable_garantia_set.all()[0],
                   h.ips_set.all()[0],
                   h, 
                  ]
    antes = {}
    despues = {}
    response = []
    for m in models_list:
        s = m
        sdic = model_to_dict(s)
        for sss in sdic.keys():
            if sdic[sss]==None:
                sdic[sss]=str('')
            else:
                try:
                    sdic[sss]=str(sdic[sss])
                except:
                    pass
        santes = set(sdic.items())-set(request.POST.items())
        dsantes = dict(santes)
        try:
            del dsantes['hardware']
            del dsantes['id']
        except:
            pass
        for kk in dsantes.keys():
            try:
                despues.update({kk:str(request.POST[kk])})
            except:
                pass
        antes.update(dsantes)
        anteses = {}
        for x in despues.keys():
            anteses.update({x:antes[x]})
    l = Log(user=request.user,hardware=h,before=simplejson.dumps(anteses),after=simplejson.dumps(despues))
    l.save()
    return True



    if request.POST['id_hardware']!='0':
        ahora = request.POST.copy()
        #ahora = simplejson.dumps(ahora)
        hh = {}
        h = Hardware.objects.get(pk=request.POST['id_hardware'])
        hh.update(model_to_dict(h))
        if len(h.network_set.all())>0:
            n = Network.objects.get(hardware=h)
            hh.update(model_to_dict(n))
        if len(h.features_set.all())>0:        
            f = Features.objects.get(hardware=h)
            hh.update(model_to_dict(f))
        if len(h.lugar_set.all())>0:
            l = Lugar.objects.get(hardware=h)
            hh.update(model_to_dict(l))
        if len(h.software_set.all())>0:
            s = Software.objects.get(hardware=h)
            hh.update(model_to_dict(s))
        if len(h.responsable_garantia_set.all())>0:
            rg = Responsable_garantia.objects.get(hardware=h)
            hh.update(model_to_dict(rg))
        before_change = {}
        changed_it = {}
        if h.db_state==1:
            for k in hh.keys():
                if k in ahora.keys():
                    ahora['respaldos'] = ','.join(request.POST.getlist('respaldos'))
                    ahora['monitores'] = ','.join(request.POST.getlist('monitores'))
                    try:
                        if str(hh[k].encode('utf-8')) != str(ahora[k].encode('utf-8')) and hh[k]!=None:
                            changed_it.update({k:ahora[k]})
                            before_change.update({k:hh[k]})
                    except:
                        None        
        else:
            None
        antes = simplejson.dumps(before_change)
        ahora = simplejson.dumps(changed_it)
        l = Log(user=request.user,hardware=h,before=antes,after=ahora)
        l.save()
    else:
        l = None
    return l

def form_to_model(modelo):
    meta = type('Meta',(),{"model":modelo,})
    KtemodelfomrKlss = type('modelform',(ModelForm,),{"Meta":meta})
    return KtemodelfomrKlss

def advanced_form_to_model(modelo,fields):
    meta = type('Meta',(),{"model":modelo,'fields':fields})
    KtemodelfomrKlss = type('modelform',(ModelForm,),{"Meta":meta})
    return KtemodelfomrKlss

def modelosXmarca(request):
    idmarca = request.POST['marca']
    if idmarca:
      modelos = Modelo.objects.filter(marca=idmarca)
      response = [{'pk':x.pk,'text':x.nombre} for x in modelos]
      modelos = simplejson.dumps(response,True)
      return HttpResponse(modelos)
    else:
      response = {model:None}
      return HttpResponse(simplejson.dumps(response,True))

def responsables(request):
    idempresa = request.POST['empresa']
    responsables = Responsable.objects.filter(empresa=idempresa)
    responsables = [{'pk':x.pk,'text':x.responsable} for x in responsables]
    response = simplejson.dumps(responsables,True)
    return HttpResponse(response)
    
def addform(request):
    marcas = Marca.objects.all()
    empresas = Empresa.objects.all()
    edificios = Edificio.objects.all()
    try:
        idhd = request.GET['id_editando']
        h = Hardware.objects.get(id=idhd)
        values_form = h
    except:
        values_form = {'pk':0}
    
    context = RequestContext(request)
    return render_to_response("pices/addform.html",
                              {'marcas':marcas,
                               'empresas':empresas,
                               'edificios':edificios,
                               'values':values_form},
                              context)


def details(request):
    try:
        idhd = request.GET['id_detail']
        h = Hardware.objects.get(id=idhd)
        values_form = h
    except:
        values_form = {'pk':0}
    
    context = RequestContext(request)
    return render_to_response("pices/detail.html",
                              {'values':values_form},
                              context)

def loggl(request,idu=None):
    if idu:
        try:
            l = Log.objects.filter(hardware=idu)
            cambios = [{'pk':x.pk,
                        'quien':x.user.first_name,
                        'cuando':x.datemodifier,
                        'ahora':simplejson.loads(x.after),
                        'antes':simplejson.loads(x.before)
                        } for x in l]
            response = l
        except:
            response = {'response':'No existe Historial para este registro'}
            cambios = None
            
    context = RequestContext(request)
    return render_to_response("pices/details/log.html",
                               {'response':response,'cambios':cambios},
                              context)

def addhardware(request):
    ips = request.POST.getlist('nameip[]')
    numbers = request.POST.getlist('ipnum[]')
    descrips = request.POST.getlist('descrip[]')
    idsips = request.POST.getlist('ips_id[]')
    
    ii = [{'nameip':ips[numbers.index(x)],'ipnum':x,
           'descrip':descrips[numbers.index(x)],
           'pk_ip':idsips[numbers.index(x)]} for x in numbers]
    discos = {}
    discos['ids'] = request.POST.getlist('id_charola')
    discos['espacio'] = request.POST.getlist('discos')
    discos['ocupado'] = request.POST.getlist('espacio_ocupado')
    discos['tipo'] = request.POST.getlist('tipo_disco')
    discos['tamanyo']=request.POST.getlist('tamanyo_disco')

    discos_info = [{'id_disco':x,
                    'espacio':discos['espacio'][discos['ids'].index(x)],
                    'ocupado':discos['ocupado'][discos['ids'].index(x)],
                    'tamanyo':discos['tamanyo'][discos['ids'].index(x)],
                    'tipo':discos['tipo'][discos['ids'].index(x)]} for x in discos['ids']]

    options = {
        'block_marca':Marca,
        'block_modelo':Modelo,
        'block_empresa':Empresa,
        'block_responsable_nombre':Responsable,
        'block_edificio':Edificio
    }
    dobled = {
        'block_infogeneral':Hardware,
        'block_network':Network,
        'block_features':Features,
        'block_responsable_garantia':Responsable_garantia,
        'block_location':Lugar,
        'block_software':Software,
    }

    keys_models = {
        'block_infogeneral':'id_hardware',
        'block_network':'id_network',
        'block_features':'id_features',
        'block_responsable_garantia':'id_responsable_garantia',
        'block_location':'id_lugar',
        'block_software':'id_software',
    }
    datapost = request.POST.copy()

    try:
        if request.POST['id_hardware']:
    		log_usuarios(request)
    except:
        None
    
    if request.POST['seccion']=='block_infogeneral':            
        evaluater = form_to_model(dobled['block_infogeneral'])
        idinstance = request.POST[keys_models['block_infogeneral']]
        if idinstance!='0':
            instanced_e = dobled['block_infogeneral'].objects.get(pk=idinstance)
        else:
            instanced_e = None
        datapostfirst = request.POST.copy()
        if datapostfirst['idocs']=='None':
            datapostfirst['idocs']=0
        evaluater = evaluater(datapostfirst,instance=instanced_e)
        
        if evaluater.is_valid():
            saved_instance = evaluater.save()
            response = {'saved block_infogeneral':saved_instance.pk}
            pkiki = saved_instance.pk
        else:
            response = evaluater.errors
            pkiki = None
            assert False,response
        
        '''
        IP PROCESO DE GUARDADO
        '''
        for ips in ii:
            if len(ips['ipnum'])>6:
                    if ips['pk_ip']: 
                        ip_a,fail = Ips.objects.get_or_create(hardware=saved_instance,id=ips['pk_ip'])
                    else:
                        ip_a,fail = Ips.objects.get_or_create(hardware=saved_instance,ipnum=ips['ipnum'])
                    ip_a.nameip=ips['nameip']
                    ip_a.ipnum=ips['ipnum']
                    ip_a.descrip=ips['descrip']
                    ip_a.save()
    
        for discx in discos_info:
                newd,nod = Disco.objects.get_or_create(hardware=saved_instance,id_charola=discx['id_disco'])
                newd.espacio_ocupado = discx['ocupado']
                newd.discos = discx['espacio']
                newd.tipo_disco = discx['tipo']
                newd.tamanyo = discx['tamanyo']
                newd.save()

        responses = {}
        for cadauno in dobled:
            if cadauno!='block_infogeneral':
                datapost['respaldos'] = ','.join(request.POST.getlist('respaldos'))
                datapost['monitores'] = ','.join(request.POST.getlist('monitores'))
                datapost['hardware']= pkiki
                optionValue = cadauno
                if datapost[keys_models[optionValue]]=='0':
                    del(datapost[keys_models[optionValue]])
                
                if optionValue in dobled:            
                    evaluater = form_to_model(dobled[optionValue])
                    idinstance = request.POST[keys_models[optionValue]]
                    if idinstance!='0':
                        instanced_e = dobled[optionValue].objects.get(pk=idinstance)
                    else:
                        instanced_e = None
                    evaluater = evaluater(datapost,instance=instanced_e)
                    if evaluater.is_valid():
                        saved_instance = evaluater.save()
                        engine_add(pkiki,datapost)
                        responses.update({'id_'+str(cadauno):saved_instance.id})
                        response = {'saved':pkiki,
                                     'who':cadauno}
                    else:
                        responses.update({'id_'+str(cadauno):evaluater.errors})
        response['elementos']=responses
        response = simplejson.dumps(response)        
            
        return HttpResponse(response)
    
    optionValue = request.POST['seccion']
    try:
        id_instance = request.POST['id']
    except:
        id_instance = '0'

    if id_instance=='0':
        instancia = None
    else:
        instancia = options[optionValue].objects.get(pk=id_instance)
    evaluater = form_to_model(options[optionValue])
    evaluater = evaluater(request.POST,instance=instancia)
    
    if evaluater.is_valid():
        response = evaluater.save()
        results = {'saved':'ok',
                   'option':optionValue,
                   'pk':response.id
                  }
        
        try:
             hopen = request.POST['hardware']
        except:
            hopen = None
        
        if hopen:
            values_search_engine = request.POST.copy()
            if optionValue=='block_infogeneral':
                hardwareid = response.id
                values_search_engine['marca']=response.marca.nombre
                values_search_engine['modelo']=response.modelo.nombre
            else:
                hardwareid = request.POST['hardware']
            engine_add(hardwareid,values_search_engine)    
            
    else:
        results = evaluater.errors
        
    results = simplejson.dumps(results,True)
    return HttpResponse(results)
    
def ending(request,idu=None):
    if idu:
        h = Hardware.objects.get(pk=idu)
        h.db_state=1
        h.save()
        response = {'changed':'ok'}
    else:
        response = {'changed':'failed'}
        l = Log(user=request.user,hardware=h,before='',after='Registro Finalizado ' + str(h.nombre))
        l.save()
    response = simplejson.dumps(response)
    return HttpResponse(response)

@login_required(login_url="/")
def deleting_h(request,idu=None):
    if idu:
        h = Hardware.objects.get(pk=idu)
        hh = model_to_dict(h)
        hh = simplejson.dumps(hh)
        l = Log(user=request.user,before=hh,after='Registro Eliminado ' + str(h.nombre))
        l.save()
        h.db_state=2
        h.save()
        response = {'removed':'Registro eliminadó con éxito.'}
    else:
        response = {'removed':'error unexpected'}
    
    response = simplejson.dumps(response)
    return HttpResponse(response)




def search_method(request):
    args = {} #{'db_state__in':(1,3)}
    data_advanced = request.POST.copy()
    titulos = data_advanced.getlist('camper')
    
    deletes = ('csrfmiddlewaretoken','liga_txt','text_srch',
               'campo','operator_date','operator_date_late',
               'dated','datedlast','first_date','last_date','camper','texto')
    
    for x in deletes:
        try:
            data_advanced.pop(x)
        except:
            pass
    #deleter = map(lambda x: data_advanced.pop(x),deletes) 
    
    for d in data_advanced:
        if len(d)>3:
            args[str(d)]=str(data_advanced[d])
    
    u = request.user
    g = [{'g':x.name,'pkg':x.pk} for x in u.groups.all()]
    txt_srch=request.POST["text_srch"]
    pices_search_txt=Q(tipo_hdwre__icontains=txt_srch) | Q(nombre__icontains=txt_srch) | Q(ips__ipnum__icontains=txt_srch) | Q(software__plataforma__icontains=txt_srch) | Q(responsable_garantia__nombre_aplicacion__icontains=txt_srch)
    results=Hardware.objects.filter(pices_search_txt)
    results=results.filter(**args).order_by('-pk')
    #results=results.filter(pices_search_txt)
    return results

@login_required(login_url="/")
def search_engine(request,items_per_page=20): 
    context = RequestContext(request)
    results = search_method(request)
    paginator = Paginator(results,items_per_page)
    profiles_open=()
    relations={'Linux':('linux',),
                'AS400':('AS400',),
                'Almacenamiento':('VTL','Controladora','Switch',),
                'Windows':('Windows',),
                'Respaldos':('Controladora',),
                'admin':('all',),
                'Reportes':('none',),
                }
    
    totals = len(results)
    for x in request.user.groups.all():
        profiles_open+=relations[str(x)]
    
    try:
        page = int(request.GET['page'])
    except:
        page = 1
    try:
        resultados = paginator.page(page)
    except:
        resultados = paginator.page(paginator.num_pages)
    
    return render_to_response("pices/list_result_search.html",{
                                                          'editables':profiles_open, 
                                                          'results':resultados,
                                                          'totales':totals,
                                                          'num_items':items_per_page},context)

@login_required(login_url='/')
def reporter(request):
    results = search_method(request)
    titulos = request.POST.getlist('camper')
    extract_data = []
    map(lambda x: extract_data.append(results.values(x)),titulos)
    response = extract_data[0]
    for i in range(1,len(extract_data)):
        for x,y in enumerate(response):
            response[x].update(extract_data[i][x])
    return HttpResponse(response)

@login_required(login_url="/")
def migration(request):
    url_import = settings.SITE_OCSINVENTORY
    u = urllib2.urlopen(url_import)
    pices = u.read()
    pices = simplejson.loads(pices)

    for p in pices:

        if p['SMODEL']=="VMware Virtual Platform":
           tipo = 'Virtual'
        else:
            tipo = ("Fisico")

        m,mno = Marca.objects.get_or_create(nombre=p['SMANUFACTURER'])
        md,mdno = Modelo.objects.get_or_create(nombre=p['SMODEL'],marca=m)
        h,hno = Hardware.objects.get_or_create(idocs=p['ID'])
        h.nombre=p['NAME']
        h.marca = m
        h.modelo = md
        h.ssn = p['SSN']
        h.tipo_hdwre=tipo
        h.db_state=3
        h.save()
        f,nof = Features.objects.get_or_create(hardware=h)
        n,non = Network.objects.get_or_create(hardware=h)
        s,nos = Software.objects.get_or_create(hardware=h)
        ipp,noipp = Ips.objects.get_or_create(hardware=h)
        ipp.nameip='IP'
        ipp.ipnum=p['IPADDR']
        ipp.save()
        f.ram=p['MEMORY']
        f.proc_socket=p['PROCESSORN']
        f.espacio_asignado=p['TOTALHD']
        f.save()
        s.plataforma=p['OSNAME']
        s.version=p['OSVERSION']
        s.service_pack=p['OSCOMMENTS']
        s.save()                

        info = {
                'nombre':p['NAME'],
                'ip':p['IPADDR'],
                'osname':p['OSNAME'],
                'version':p['OSVERSION'],
                'modelo':p['SMODEL'],
                'marca':p['SMANUFACTURER']
                }

        info = simplejson.dumps(info,True)
        srh_engine,failedd = SearchEngine.objects.get_or_create(hardware=h)
        srh_engine.texto = info
        srh_engine.save()
    response = {'import':'ok',
               'cantidad':len(pices)}
    response = simplejson.dumps(response)             
    return HttpResponse(response)

def autofill(request):
    f = open('/var/www/inventario/inventario.csv','r')
    lineas = tuple(f)
    f.close()
    names = {}
    for linea in lineas:
        l = linea.split('\t')
        if l[0]!='NOMBRE' and len(l)>19:
            try:
                h,failh = Hardware.objects.get_or_create(nombre=l[0],ssn=str(l[19]))
                ''' PARA ELPROCESO DE LUGAR '''
                lugar,fail = Lugar.objects.get_or_create(hardware=h)
                edificio,fail = Edificio.objects.get_or_create(nombre_edificio=str(l[9]))
                lugar.edificio = edificio        
                lugar.formato_nombre = l[15].upper()
                lugar.fila_rack = str(l[10])
                lugar.num_rack = str(l[11])
                lugar.save()
                ''' PARA EL PROCESO APICACION RESPONSABLE GARANTIA '''
                rg,fail = Responsable_garantia.objects.get_or_create(hardware=h)
                empresa,fail = Empresa.objects.get_or_create(empresa=l[5])
                responsable,fail = Responsable.objects.get_or_create(responsable=l[29],empresa=empresa)
                rg.nombre_aplicacion = str(l[4])
                rg.empresa = empresa
                rg.area = str(l[8]).upper() 
                rg.responsable = responsable
                rg.tipo_soporte = l[32]
                rg.save()
                ''' MONITOREO Y RESPALDOS '''
                nt,fail = Network.objects.get_or_create(hardware=h)
                nt.monitores = l[27].upper()
                nt.respaldos = l[28].upper()
                if l[20]!='N/A':
                    ips,fail = Ips.objects.get_or_create(hardware=h,nameip='ip ilo')
                    ips.numip = l[20]
                    ips.descrip = 'PASSWD: '+str(l[21])
                    ips.save() 
                nt.save()
            except ValueError,err:
                rg = 0
    return HttpResponse(rg)
