# -*- encoding: utf-8 -*-
from models import * 
import simplejson


class FoodListManager:
    def __init__(self,params):
        self.params = params

    def savelist(self):
        demand = self.params.POST
        nombre = demand["nombre"]
        poder = demand["poder"]
        porcion = demand["porcion"]
        #vegy = demand["vegy"]
        if demand["idpadre"]:
            padre = demand["idpadre"]
        else:
            padre = 0
        nf,ff = Foodlist.objects.get_or_create(nombre=nombre,
                                            poder=poder,
                                            porcion=porcion,
                                            idpadre=int(padre),
                                            userpub=self.params.user)
        return nf
    
    def editlist(self):
        parame = self.params.POST
        pker = parame["idalimento"]
        try:
            nf = Foodlist.objects.get(pk=pker)
        except:
            return "fuck"
        
        nombre = parame["nombre"]
        poder = parame["poder"]
        porcion = parame["porcion"]
        #vegy = demand["vegy"]
        if parame["idpadre"]:
            padre = parame["idpadre"]
        else:
            padre = 0

        nf.nombre = nombre
        nf.porcion = porcion
        nf.poder = poder
        nf.padre = padre
        nf.save()
        responseC = {'ok':'ok'}
        response = simplejson.dumps(responseC)
        return response



class Foodmanager:

    
    def __init__(self,params):
        self.params = params

        '''
        Al momento que generamos un plan alimenticio este tiene un estatus 0 hasta que el manager decide finalizar con su captura
        el plan tiene lso siguientes estatus: 
                0 Editandose
                1 finalizado
                2 asignado a algun grupo
                3 finalizo su periodo
                4 eliminado

        '''
    def planabierto(self):


        '''
        verifica si hay un plan en modo edición, de ser asi continuaremos con la tarea
        de lo contrario podremos crear un plan nuevo con las caracteristicas indicadas
        '''


        planeditandose = Planinfo.objects.filter(status=0).select_related()
        if planeditandose:
                return planeditandose
        else: 
                return None
    
    def savePlan(self):
            newplan,doit = Planinfo.objects.get_or_create(nombre=self.params["nombre"],
                                  usuariopub=self.params["user"])
            saved = newplan
            return saved
            

    def workPlanParts(self):
            pices = self.params.POST
            foodlist = pices.get('foodtime_list').split(",")
            #assert False,pices.get('foodtime_list')
            if(pices.get('foodtime_list')):
                despices = [{'pik':str(x),
                         'list':pices.get('foodtime_list'),
                         'info':{'pk':str(x),
                        'nombre':pices.get('name_food_'+str(x)),
                        'cantidad':pices.get('food_' + str(x)),
                        'extra':pices.get('extra_food_' + str(x)),
                        'remplazable':pices.get('remplaza_' + str(x))
                       }}
                    for x in foodlist]
            
                despices = simplejson.dumps(despices)
            else:
                despices = None
            
            dia = pices.get('deldia')
            colacion = pices.get('colation')
            texto = pices.get('foodtime')
            idplan = self.params.POST.get('planid')
            planid = Planinfo.objects.get(pk=idplan)
            if pices.get('foodtime_list'):
                nwplan,idp = Planpices.objects.get_or_create(texto=texto,dia=dia,status=1,planid=planid) 
                nwplan.alimento=despices;
                nwplan.save()
            return (despices)

    def delpplanfull(self):
        planv = Planinfo.objects.get(pk=self.params)
        planv.delete()
        return {'delete':'ok'}
    def delplan(self):
            parts = self.params
            response = None
            try:
                planpice = Planpices.objects.get(pk=int(self.params))
                planpice.delete()
                response = True
            except:
                response = None
            return response

    def endplan(self):
        response = None
        try:
            idplan = self.params
            plan = Planinfo.objects.get(pk=idplan)
            plan.status = 2
            plan.save()
            response = True
        except:
            response = "Error"
        return response

    def planEditor(self):
        try:    
            planeditando = Planinfo.objects.get(pk=int(self.params["plan"]))
            planeditando.status = 0
            planeditando.save()
            response = True
            
        except:
            response = None
        return response
