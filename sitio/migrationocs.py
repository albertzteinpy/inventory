import MySQLdb as mdb
@login_required(login_url="/")
def migration(request):
    con = mdb.connect('localhost', 'root', 'patas', 'ocsweb');
    with con: 
        cur = con.cursor()
        cur.execute("SELECT DISTINCT SMANUFACTURER as marca FROM bios")
        rows = cur.fetchall()
        for row in rows:
            if row!='':
                r,failedd = Marca.objects.get_or_create(nombre=str(row[0]))
    # FORM MODEL CATALOGE
        cur = con.cursor()
        cur.execute("SELECT DISTINCT(SMODEL),SMANUFACTURER FROM bios")
        rows = cur.fetchall()
        for r in rows:
            m = Marca.objects.get(nombre=r[1])    
            modelo,failedd = Modelo.objects.get_or_create(marca=m,nombre=r[0])
    #FROM HARDWARE
        cur = con.cursor()
        query_str = "select H.ID,H.NAME,SMANUFACTURER,SMODEL,SSN,OSNAME, \
                    OSVERSION,H.OSCOMMENTS,GROUP_CONCAT(IPADDRESS SEPARATOR '\n') as IP, \
                     H.MEMORY,H.PROCESSORN,H.PROCESSORT \
                    from ocsweb.hardware H inner join ocsweb.bios B  \
                     LEFT JOIN ocsweb.networks N using(HARDWARE_ID) \
                     where H.ID = B.HARDWARE_ID  GROUP BY B.HARDWARE_ID "
        cur.execute(query_str)
        rows = cur.fetchall()
        for r in rows:
            marcaid = Marca.objects.filter(nombre=r[2])[0]
            modeloid = Modelo.objects.filter(nombre=r[3])[0]
            if r[3]=="VMware Virtual Platform":
                tipo = 'Virtual'
            else:
                
                tipo = ("Fisico")
                
            h,failedd = Hardware.objects.get_or_create(idocs=r[0])
            h.nombre=r[1]
            h.ssn=r[4]
            h.marca=marcaid
            h.modelo=modeloid
            h.tipo_hdwre=tipo
            h.unidades=0
            h.db_state=3
            h.save()
            s,failedd = Software.objects.get_or_create(hardware=h)
            s.plataforma=str(BeautifulSoup(r[5]))
            s.version=str(BeautifulSoup(r[6]))
            s.release=''
            s.service_pack=str(r[7])
            s.save()                                               
            n,failedd = Network.objects.get_or_create(hardware=h)
            n.ip=r[8]
            n.puerto_switch=0
            n.cluster=1
            n.save()
                                              
            featurs,failedd = Features.objects.get_or_create(hardware=h)
            featurs.ram=r[9]
            featurs.proc_socket=r[10]
            featurs.proc_arq=r[11]
            featurs.save()
            info = {
                    'nombre':str(r[1]),
                    'ip':str(r[8]),
                    'osname':str(BeautifulSoup(r[5])),
                    'version':str(BeautifulSoup(r[6])),
                    'modelo':r[3],
                    'marca':r[2]
            }
            info = simplejson.dumps(info,True)
            srh_engine,failedd = SearchEngine.objects.get_or_create(hardware=h)
            srh_engine.texto = info
            srh_engine.save()
    return HttpResponse('completo')
