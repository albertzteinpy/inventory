# -*- coding: utf_8 -*-
# -*- encoding: utf-8 -*- 
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from sitio.models import *
from django.forms import ModelForm
import simplejson
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required, permission_required
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from inventario_views import form_to_model
import datetime


def change_group(u,g):
    try:
        grp=Group.objects.get(pk=g)
        ug=User.groups.through.objects.get(user=u)
        ug.delete()
        #ug.save()
        changed=True
    except:
        changed=False
    return changed

@login_required(login_url="/")
def index(request,idu=None):
    context = RequestContext(request)
    usuarios = User.objects.all().order_by('-pk')
    grupos = Group.objects.all()
    yo = request.user
    gp = [(x.name) for x in request.user.groups.all()]
    return render_to_response("main_usuario.html",{'usuarios':usuarios,
                                                   'usr':None,
                                                   'grupos':grupos,
                                                   'gp':gp,
                                                   'yo':yo},context)
@login_required(login_url="/")
def addinguser(request):
    hoy = datetime.date.today()
    if request.POST:
        postes = request.POST.copy()
        if postes['idu']:
            usr,failedd = User.objects.get_or_create(pk=postes['idu'])            
        else:
            usr,failedd = User.objects.get_or_create(username=postes['username'])
        if postes['password']:
            usr.set_password(postes["password"])
        usr.email = postes['username']
        usr.first_name  = postes['first_name']
        usr.last_name = postes['last_name']
        usr.is_active = True
        usr.is_staff = True
        post_group=request.POST.getlist("groups")
        ug=User.groups.through.objects.filter(user=usr)
        #assert False,ug
        ug.delete()
        for group_n in post_group:    
            usr.groups.add(int(group_n))
            if group_n=='1':
                usr.is_superuser=True
        
        usr.save()
        
        response = {'ok':'saved','pk':usr.id}
    else:
        response = {'failed':True}

    response = simplejson.dumps(response,True)
    return HttpResponse(response)


@login_required(login_url="/")
def deluser(request,idu=None):
    if idu:
        u = User.objects.get(pk=idu)
        nameu = u.username
        u.delete()
    response = {'delete':'ok',
                'pk':idu,
                'name':nameu}
    response = simplejson.dumps(response,True)
    return HttpResponse(response)


@login_required(login_url="/")
def userform(request,idu=None):
    context = RequestContext(request)
    
    if idu:
        editando = User.objects.get(pk=idu)
    else:
        editando = None
    grupos = Group.objects.all()
    gp = [(x.name) for x in request.user.groups.all()]
    return render_to_response("pices/forms/userform.html",{'usr':editando,'grupos':grupos,'gp':gp},context)
    
