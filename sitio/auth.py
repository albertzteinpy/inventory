from django.contrib.auth.models import User
from django.contrib.auth import authenticate
import simplejson

class AuthManager:
    ouathV = None
    
    def __init__(self,params):
        self.params = params
    
    def is_user(self):
        try:
            us = User.objects.get(username=self.params[0])
        except:
            us = None
            
        if us.is_superuser:
            user = authenticate(username=us.username, password=self.params[1])
            if user:
                return user
            else:
                return None
        else:
            return None
    
    def is_pathient(self):
        params = self.params.POST
        try:
            us = User.objects.get(email=params.get("correo"))
        except:
            us = None            
        if us:
            user = authenticate(username=us.username, password=params.get("clave"))
            return user
        else:
            return None
