# -*- coding: utf_8 -*-
# -*- encoding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

class Marca(models.Model):
    nombre = models.CharField(u'Marca',max_length=100)

class Modelo(models.Model):
    marca = models.ForeignKey(Marca)
    nombre = models.CharField(u'Marca',max_length=100)

class Tipo_hdwre(models.Model):
    nombre = models.CharField('',max_length=100)

class Hardware(models.Model):
    idocs = models.IntegerField(u'Clave OSC Inventory',blank=True,null=True)
    nombre = models.CharField(u'Nombre Hardware',max_length=150,null=True,blank=True)
    cluster = models.CharField(u'Cluster',max_length=50,blank=True,null=True)
    ssn = models.CharField(u'Numero de serie',max_length=150,null=True,blank=True)
    marca = models.ForeignKey(Marca,blank=True,null=True)
    modelo = models.ForeignKey(Modelo,blank=True,null=True)
    posicion = models.CharField(u'Posicion',max_length=100,blank=True,null=True)
    ambiente = models.CharField(u'Ambiente',max_length=150,blank=True,null=True)
    lun = models.CharField(u'Lun',max_length=100,blank=True,null=True)
    consola = models.CharField(u'Consola',max_length=50,blank=True,null=True)
    num_activo = models.CharField(u'Nm. Activo',max_length=120,blank=True,null=True)
    cmdb = models.BooleanField(u'CMDB',default=True)
    tipo_hdwre = models.CharField(u'Tipo de Hardware',max_length=120,blank=True,null=True)
    #  define el status del registro {1:activo,2:eliminado,3:edicion,4:ocs}
    db_state = models.IntegerField(u'Status',default=3,blank=True,null=True)
    fecha_registro = models.DateField(u'Fecha Registro',auto_now=True)
    fecha_llegada = models.DateField(u'fecha llegada',blank=True,null=True)
    fecha_instalacion = models.DateField(u'fecha instalacion',blank=True,null=True)
    fecha_apagado = models.DateField(u'fecha apagado',blank=True,null=True)


class Disco(models.Model):
    hardware=models.ForeignKey(Hardware)
    id_charola = models.CharField('',max_length=20,blank=True,null=True)
    tipo_disco = models.CharField('',max_length=100,blank=True,null=True)
    discos = models.CharField('',max_length=100,blank=True,null=True)
    espacio_ocupado = models.CharField('',max_length=100,blank=True,null=True)
    tamanyo_disco = models.CharField('',max_length=20,blank=True,null=True)

class Ips(models.Model):
    hardware = models.ForeignKey(Hardware)
    ipnum = models.CharField('',max_length=150,blank=True,null=True)
    nameip = models.CharField('',max_length=150,blank=True,null=True)
    descrip = models.CharField('',max_length=150,blank=True,null=True)

class Netcard(models.Model):
    hardware = models.ForeignKey(Hardware)
    cardname = models.CharField('',max_length=120,blank=True,null=True)
    speedcard = models.CharField('',max_length=120,blank=True,null=True)

class Network(models.Model):
    hardware = models.ForeignKey(Hardware)
    #cluster = models.CharField('',max_length=50,blank=True,null=True)
    puertos_total = models.IntegerField(u'Puertos Totales',blank=True,null=True)
    puertos_licenciados = models.IntegerField(u'Puertos Licenciados',blank=True,null=True)
    puertos_usados = models.TextField(u'Puertos Usados',blank=True,null=True)
    puerto_switch = models.IntegerField(u'Puerto Switch',blank=True,null=True)
    respaldos = models.CharField(u'Respaldos',max_length=250,blank=True,null=True)
    monitores = models.CharField(u'Monitores',max_length=250,blank=True,null=True)
    no_serie_oa = models.CharField(u'Nmero de serie OA',max_length=100,blank=True,null=True)
    
class Features(models.Model):
    hardware = models.ForeignKey(Hardware)
    ram = models.CharField(u'Memoria Ram',max_length=150,blank=True,null=True)
    espacio_asignado = models.TextField(u'Espacio Asignado',blank=True,null=True)
    antivirus = models.CharField('',max_length=150,blank=True,null=True)
    delegado_prod = models.CharField(u'Delegado Prod',max_length=150,blank=True,null=True)
    #    INFO DE PROCESADORES
    proc_socket = models.CharField(u'Socket Procesador',max_length=150,blank=True,null=True)
    proc_cores = models.CharField(u'Cores',max_length=150,blank=True,null=True)
    proc_arq = models.CharField(u'Arquitectura Procesador',max_length=150,blank=True,null=True)

class Empresa(models.Model):
    empresa = models.CharField('',max_length=150)

class Responsable(models.Model):
    responsable = models.CharField('',max_length=220)
    empresa = models.ForeignKey(Empresa)

class Responsable_garantia(models.Model):
    hardware = models.ForeignKey(Hardware)
    nombre_aplicacion = models.CharField(u'Nombre de la aplicacion',max_length=150,blank=True,null=True)
    responsable = models.ForeignKey(Responsable,blank=True,null=True)
    area = models.CharField('Area',max_length=120,blank=True,null=True)
    empresa = models.ForeignKey(Empresa,blank=True,null=True)
    tipo_soporte = models.CharField(u'Tipo de Soporte',max_length=250,blank=True,null=True)
    fecha_inicio = models.DateField(u'Fecha Inicio',blank=True,null=True)
    fecha_fin = models.DateField(u'Fecha Fin',blank=True,null=True)

#LUGARES
class Edificio(models.Model):
    nombre_edificio = models.CharField('',max_length=100)
    def racks(self):
        rs = self.lugar_set.values('num_rack').distinct()
        return rs


class Lugar(models.Model):
    hardware = models.ForeignKey(Hardware,blank=True,null=True)
    edificio = models.ForeignKey(Edificio,blank=True,null=True)
    num_rack = models.CharField(u'Numero de Rack',max_length=50,blank=True,null=True)
    fila_rack = models.CharField(u'Fila Rack',max_length=50,blank=True,null=True)
    unidades = models.CharField(u'Us',max_length=20,blank=True,null=True)
    posicion_enclousure = models.CharField(u'Posicion Enclousure',max_length=150,blank=True,null=True)
    formato_nombre = models.CharField(u'Formato',max_length=150,blank=True,null=True)
    no_enclousure = models.CharField(u'Enclousure',max_length=100,blank=True,null=True)


class Software(models.Model):
    hardware = models.ForeignKey(Hardware)
    plataforma = models.CharField(u'Sistema Operativo',max_length=150,blank=True,null=True)
    version = models.CharField(u'Version',max_length=150,blank=True,null=True)
    release = models.TextField(u'Realease',blank=True,null=True)
    service_pack = models.CharField(u'Service Pack',max_length=100,default=False,blank=True,null=True)


class SearchEngine(models.Model):
    hardware = models.ForeignKey(Hardware)
    texto = models.TextField('')


class Log(models.Model):
    user = models.ForeignKey(User)
    datemodifier = models.DateField(auto_now=True)
    hardware = models.ForeignKey(Hardware,null=True,blank=True)
    before = models.TextField()
    after = models.TextField()


class Rack(models.Model):
    edificio = models.ForeignKey(Edificio)
    uds = models.IntegerField('')
    marck_rack = models.CharField('',max_length=100,blank=True,null=True)
    distribution = models.TextField('')
    configuration = models.TextField('')
    marca = models.ForeignKey(Marca)
    modelo = models.ForeignKey(Modelo)
    desc = models.TextField('')
