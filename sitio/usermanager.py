# -*- encoding: utf-8 -*-
from models import UserPices
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
import simplejson

class UserManagement:
    def __init__(self,params):
        self.params = params
        
    def adduser(self):
        params = self.params
        
        if params["pk"]:
            user = User.objects.get(pk=params["pk"])
        else:
            user,doit = User.objects.get_or_create(username=params["correo"])      
        user.first_name = params["nombre"]
        user.last_name = params["apelos"]
        user.email = params["correo"]
        if params["passwd"]:
            user.set_password(params["passwd"])
        user.is_active = True
        user.is_superuser = False
        try:
            user.groups.add(params["grupo"])
        except:
            ug = None
   
        user.save()
        return user

    def deluser(self):
        pk = int(self.params)
        user = User.objects.get(pk=pk)
        user.delete()
        return {'ok':'delete'+str(self.params)}

    def addgroup(self):
        params = self.params
        grupo,g = Group.objects.get_or_create(name=params["grupo"])
        grupo.save()
        solved = {'grupo':grupo,'g':g}
        return solved
    
    def editgroup(self):
        params = self.params
        grupo = Group.objects.get(pk=params["idgrupo"])
        grupo.name=params["grupo"]
        grupo.save()
        solved = {'grupo':grupo,'g':'true'}
        return solved
        