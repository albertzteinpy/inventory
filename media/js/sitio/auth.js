var AuthUser = function(){
    
   this.submitlog = function(e){
      e.preventDefault();
      $(".err").hide('slow');
      if ($("#correo_txt").val()=="correo") {
         $(".err").text("Escriba un correo valido").show('slow');
      }
      var data = $(this).serializeArray();
     $.ajax({url:'login',
            data:data,
            type:'post',
            dataType:'json',
            success:function(response){
                 if(response.ok==false)
                 {
                     $(".err").text('Usuario y/o Contrseña incorrecto(s)').show('slow');
                 }
                 else if (response.ok==true) {
                     window.location="/welcome";
                 }
            }
     });
    } 
    
}
$(document).ready(function(){
    var usr = new AuthUser();
    $("#login_form").submit(usr.submitlog);    
});