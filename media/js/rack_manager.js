




$(document).ready(function(){
    
    
    
    $("#add_uds").click(function(e){
        e.preventDefault();
        $("#rack").html('');
        var uds_num = $("#uds").val();
        if(!isNaN(uds_num)){
            for(i=uds_num;i>0;i--) {
                var div_ud = document.createElement('div');
                div_ud.id = "uds_"+i;
                div_ud.className = "ud void";
                div_ud.innerHTML = '<div class="inline_blocke tiket">'+i+'</div>';
                var select_sp = document.createElement('select');
                select_sp.id = "uds_options_"+i;
                select_sp.name = "uds_options_"+i;
                $(select_sp).append('<option value="disponible">Disponible</option>');
                $(select_sp).append('<option value="pdu">PDU</option>');
                $(select_sp).append('<option value="pp">PATCH Panel</option>');
                $(select_sp).append('<option value="pcmm">Comunicación</option>');
                $(select_sp).append('<option value="fs">Fuera de servicio</option>');
                $(select_sp).append('<option value="other">Otro</option>');
                $(div_ud).append(select_sp).append('<div class="inline_blocke"><input type="checkbox" class="chx_'+i+'" name="blade" value="'+i+'"/> </div>');
                $("#rack").append(div_ud);
            }
        }
    });
    
    
    $(".blade_maker").click(function(e){
        e.preventDefault();
        var cuantos = $("[name=blade]:checked");
        var el_primero = $("[name=blade]:checked:first").val();
        var id_item = "blade_"+el_primero;
        var blade_structure = $(this).attr("id").replace("to_","");
        $("#uds_"+el_primero).before('<div id="'+id_item+'" class="inline_blocke" style="text-align:center;">bbb</div>');
        $("#"+id_item).html($("#"+blade_structure).html()).css("width","100%").css("height","100%");
        $.each(cuantos,function(x,y){
            var pices = y.value;
            $("#uds_"+pices).remove();
            var div_ud = document.createElement('div');
            div_ud.id = "uds_"+y.value;
            div_ud.className = "ud void";
            div_ud.innerHTML = y.value;
            $("#"+id_item+" .pix").append(div_ud);
        });
        //alert(el_primero);
        $("#"+id_item+" .b1").css("height","100%");
        

    });
    
    
});