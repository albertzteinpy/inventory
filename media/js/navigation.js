$.fn.senduser = function(){
    $(this).submit(function(e){
        e.preventDefault();
        var data = $(this).serializeArray();
        $.post("/addinguser",
                        data,
                        function(response){
                        
                        if(response.ok=='saved')
                        {
                            $("#result_box").load("/users/");
                        }
                        
                        },
                        'json');
        
    });
    
}

$.fn.deleting = function(){
 
    $(this).click(function(e){
        e.preventDefault();
        var namev = $(this).attr("href");
        var accept_delete = confirm("esta seguro que desea borrar este usuario:" + namev);
        if (accept_delete){
            var victim = $(this).attr("id").replace('kel_','');
            var url_x = "/deluser/"+victim+"/";
            $.ajax({url:url_x,dataType:'json',success:function(response){
                    $("#kel_"+response.pk).parents(".each_one:first").remove().html('Se eliminó con éxito'+response.name);
                }    
            });            
        }
    });
     

}




jQuery.successed = function(block,response){
        this.response = response;
        if (response.saved=='ok') {
                alert("Registro  guardado con éxito.");
                $(".err").remove();
                instancia = response.option.replace('block','id').replace('infogeneral','hardware');
                $("#"+instancia).val(response.pk);
                $(".errform").remove();
        }
        else{
                $(".errform").remove();
                if(block!='block_infogeneral' && $("#id_hardware").val()==0){
                        alert('Debe agregar la Información General primero.');
                }
                $.each(response,function(x,y){
                        var span_er = document.createElement('span');
                        span_er.className = 'inline_blocke errform';
                        span_er.innerHTML = y;
                        $("#"+block+" #"+x).parent('div:first').after(span_er);
                });
                $(".errform").show('fast');
        }
}

var nums = new Array(47,48,49,50,51,52,53,54,55,56,57);
var defaulgts = new Array(8,17,46,9);
var keys_restrict = {
                'ip':[47,48,49,50,51,52,53,54,55,56,57,190,13,8,17,46,9,96,97,98,99,100,101,102,103,104,105,96,110],
                'ip_ilo':[47,48,49,50,51,52,53,54,55,56,57,190,13,8,17,46,9,96,97,98,99,100,101,102,103,104,105,96,110],
                'rack':[47,48,49,50,51,52,53,54,55,56,57,190,13,8,17,46,9,96,97,98,99,100,101,102,103,104,105,96,110],
                'fila':[47,48,49,50,51,52,53,54,55,56,57,190,13,8,17,46,9,96,97,98,99,100,101,102,103,104,105,96,110]
}


var likes = 
        {
        'notmain':{'dislikes':['',''],'modules':null},
        
        'Fisico':{'dislikes':['lun','consola','puertos_total','puertos_licenciados','puertos_usados'
                              ,'puerto_switch','cluster','serie_rack','no_serie_oa'],
                  'modules':['block_caja_discos']},
        
        'Virtual':{'dislikes':
                        ['ssn','unidades','feche_llegada','fecha_aplicacion','lun',
                         'fecha_apagado','modelo','marca',,'cluster',
                         'puertos_total','puertos_licenciados','puertos_usados','puerto_switch','serie_rack',
                         'no_enclousure','no_serie_oa','ip_ilo','pw_ilo','fecha_inicio','fecha_fin'],
                  'modules':['to_block_location','block_caja_discos']},
                  
        'Controladora':{'dislikes':['lun','consola','formato_nombre','ip_ilo','pw_ilo','espacio_asignado',
                                    'puertos_total','puertos_licenciados','puertos_usados','puerto_switch',
                                    'antivirus','proc_cores','serie_rack','no_enclousure','no_serie_oa','posicion_enclousure'],
                        'modules':['to_block_software']},
        
        'Switch':{'dislikes':['cmdb','lun','consola','formato_nombre','ip_ilo','pw_ilo','cluster','antivirus',
                              'proc_cores','serie_rack','no_enclousure','no_serie_oa'],
                  'modules':['to_block_software','block_caja_discos']},
        
        'Enclousure':{'dislikes':['cmdb','lun','consola','ram','formato_nombre','ip','respaldos','cluster',
                                  'puertos_total','puertos_licenciados','puertos_usados','puerto_switch','antivirus',
                                  'proc_cores','espacio_asignado','proc_socket','proc_arq','monitores','no_enclousure','posicion_enclousure'],
                      'modules':['block_caja_discos']},
        
        'VTL':{'dislikes':['cmdb','lun','consola','ram','formato_nombre','ip_ilo','pw_ilo','cluster',
                           'respaldos','monitores','puertos_total','puertos_licenciados','puertos_usados',
                           'puerto_switch','antivirus','proc_cores','serie_rack','no_enclousure',
                           'no_serie_oa','espacio_asignado','plataforma','service_pack','posicion_enclousure'],
                      'modules':['to_block_features','block_caja_discos']},
        'AS400':{'dislikes':['service_pack','antivirus','puertos_total','no_enclousure',
                             'posicion_enclousure','no_serie_oa','serie_rack',
                             'puertos_usados','posicion_enclousure','proc_socket','cluster','puertos_licenciados','release',
                             'puerto_switch','antivirus','consola','lun','service_pack'],'modules':['block_caja_discos']}
        
        };
        
function validation(bloque){
        $(".errvoid").removeClass('errvoid');
        var bloque = $("#stepp").text();
        var is_valid = null;
        var errrs = 0;
        var pep = "";
        /*
        sectores = new Array('info_general','features');
        if(bloque){
                sector = "#"+sectores[bloque];
        }
        else   sector = "";
        */
        var paso = new Array('block_infogeneral','block_features','block_network',
                            'block_responsable_garantia','block_location',
                            'block_software');
        var texterr = ''; 
        if(paso[bloque-1]){
            input_total = $("#add_hardware_form #"+paso[bloque-1]+" :input").not('.closeded').not('.optional').serializeArray();        
        }
        else{
            alert("error inesperado, intente nuevamente.");
            return false;    
        }
        var tipo_hardware = $("#tipo_hdwre option:selected").val();
        if(tipo_hardware)  unables = likes[tipo_hardware].dislikes;
        else unables = null;
        if(1==1){                
                $.each(input_total,function(x,y){
                        if(y.value==''){
                                errrs++;
                                texterr=texterr+y.name+' no es información valida.\n';
                                $("#"+y.name).addClass('errvoid');
                                
                                pep += ',' + y.name ;
                        }
                });
                
                if (errrs>0){
                        is_valid=false;
                        $(".response_false").append('\n'+texterr);
                }
                else{ 
                    is_valid = true;
                }
                
        }
        else{
               is_valid = null; 
        }

        return is_valid;
}

function dislikes_module(param) {
                $(".closeded").show('fast').removeClass('closeded');
                if(param=='None'){
                        var elementos = "nada por hacer";        
                }        
                else if(typeof(param)=='string'){
                      var elementos = (likes[param]['dislikes']);
                      var modules_dis = (likes[param]['modules']);
                }
                else{
                        var elementos = (likes[$(param).val()]['dislikes']);
                        var modules_dis = (likes[$(param).val()]['modules']);
                }
                if(elementos){
                        $.each(elementos,function(x,y){
                                if(y) $("#"+y).addClass('closeded').val('').parent("div:first").hide('fast').addClass('closeded');        
                        });
                }
                if(modules_dis){
                        $.each(modules_dis,function(x,y){
                            var padre = $("#"+y).attr("id").replace('to_','');
                                if(y){
                                    $("#"+y).hide('fast').addClass('closeded');
                                    $("#"+padre+" :input").addClass('closeded');
                                }
                        });
                }
                var paso = 1; 
                $(".shower").not(".closeded").each(function(){
                        $(this).attr("href","#"+paso);
                        paso++;
                });
}

$.fn.likes_dislikes = function(){
        this.change(function(){
                dislikes_module(this);
                $(".errvoid").removeClass();
        });
}


var Ajaxing = function(params){
    
    this.params = params;

    this.sendJX = function(){
        $("#successsadd").text(' ');
        $.ajax({url:this.params.url,
                type:'POST',
                dataType:'json',
                data:this.params.data,
                error:function(response){
                        $.successed(response);
                },
                success:function(response){
                    $("#add_hardware_form .errform").remove();
                    if(response.saved=='ok'){
                        if(response.option=='block_marca'){
                            var op = document.createElement('option');
                                op.value = response.pk;
                                op.innerHTML = $("#singletext").val();
                                $("#marca").append(op);
                                $("#marca").val(response.pk);
                                $("#seccion").val('block_infogeneral');
                                
                        }
                        if(response.option=='block_modelo'){
                            var op = document.createElement('option');
                                op.value = response.pk;
                                op.innerHTML = $("#singletext").val();
                                $("#modelo").append(op);
                                $("#modelo").val(response.pk);
                                $("#seccion").val('block_infogeneral');
                                
                        }
                        if (response.option=='block_edificio') {
                            var op = document.createElement('option');
                                op.value = response.pk;
                                op.innerHTML = $("#singletext").val();
                                $("#edificio").append(op);
                                $("#edificio").val(response.pk);
                                $("#seccion").val('block_infogeneral');
                                
                                
                        }
                        if (response.option=='block_empresa') {
                            var op = document.createElement('option');
                                op.value = response.pk;
                                op.innerHTML = $("#singletext").val();
                                $("#empresa").append(op);
                                $("#empresa").val(response.pk);
                                $("#seccion").val('block_infogeneral');
                                
                                
                        }
                        if (response.option=='block_responsable_nombre') {
                            var op = document.createElement('option');
                                op.value = response.pk;
                                op.innerHTML = $("#singletext").val();
                                $("#responsable").append(op);
                                $("#responsable").val(response.pk);
                                $("#seccion").val('block_infogeneral');
                        }
                        else{
                            //$("#successsadd").text('AGREGADO CON ÉXITO');
                            if(response.option=='block_infogeneral'){
                                $("[name=hardware]").val(response.pk);
                            }
                            else{
                                $("#"+response.option+" .idd").val(response.pk);
                            }
                        }
                        $("#save_single").unbind();
                        $("#singletext").val('');
                        $("#block_singleton").hide('fast');
                        
                    }
                    else{
                        if(response.hardware){
                            alert("Debe Capturar la información general.");    
                        }
                        else{    
                            $.each(response,function(x,y){
                                var span_err = document.createElement('span');
                                span_err.className = "inline_blocke errform";
                                span_err.innerHTML = y;
                                $("[name="+x+"]").parent('div:first').after(span_err);
                            });
                            $("#add_hardware_form .err").show('fast');                        
                        }
                    }
                }
            });
    }
}



var Summiter = function(){
        this.singleSrValues = function(e){
                e.preventDefault();
                var pasofinal = $(".shower").not(".closeded").length;
                var paso = $("#stepp").text();
                var sevale = validation(paso);
                //if (sevale && pasofinal && paso==pasofinal) linea para validar pasos/ funcionalidad rechazada por el usuario
                if(sevale){
                        var data_form = $("#add_hardware_form").not('.closeded').serializeArray();
                        $.post("/addhardware",
                        data_form,
                        function(response){
                            $(".response_ok,.response_false").remove();
                        if (response.saved) {
                                $("#pasos").before('<h2 class="response_ok">Información Guardada con éxito.</h2>');
                                //alert('Registro agregado con éxito.');

                                if(response.elementos){
                                    $("#id_software").val(response.elementos.id_block_software);
                                    $("#id_features").val(response.elementos.id_block_features);
                                    $("#id_lugar").val(response.elementos.id_block_location);
                                    $("#id_responsable_garantia").val(response.elementos.id_block_responsable_garantia);
                                    $("#id_network").val(response.elementos.id_block_network);
                                }
                                $("#id_hardware").val(response.saved);
                                var nuevoreg = document.createElement('a');
                                $("#save").text("EDITAR");
                                if($("#new_save").length<1){
                                    nuevoreg.href="#nuevo";
                                    nuevoreg.className="new inline_blocke";
                                    nuevoreg.id="new_save";
                                    nuevoreg.innerHTML="NUEVO REGISTRO";
                                    $(nuevoreg).click(function(){
                                        $("#add_buton").click();
                                        return false;
                                        formid = document.getElementById('add_hardware_form');
                                        formid.reset();
                                        return false;
                                        $(this).remove();
                                        $("#save").text("GUARDAR");
                                    });
                                    $("#botonera_form").append(nuevoreg);
                                }
                            }
                        },
                        'json');
                                
                }
                else{
                        $(".response_ok,.response_false").remove();
                        $("#pasos").before('<h2 class="response_false">La información no esta completa.</h2>');
                        //alert("la información no esta completa.");
                }
        }
        
        this.addMarca = function(){
            var sendToWS = $("#add_hardware_form").attr("action");
            var data = {'seccion':'block_marca',
                          'nombre':$("#singletext").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                        }
            var params = {data:data,url:sendToWS};
            var JX = new Ajaxing(params);
                JX.sendJX();
                

        }
        this.addModelo = function(){
            var sendToWS = $("#add_hardware_form").attr("action");
            var data = {'seccion':'block_modelo',
                          'nombre':$("#singletext").val(),
                          'marca':$("#marca option:selected ").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                        }
            var params = {data:data,url:sendToWS};
            var JX = new Ajaxing(params);
                JX.sendJX();


        }
        this.addEdificio = function(){
            var sendToWS = $("#add_hardware_form").attr("action");
            var data = {'seccion':'block_edificio',
                          'nombre_edificio':$("#singletext").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                        }
            var params = {data:data,url:sendToWS};
            var JX = new Ajaxing(params);
                JX.sendJX();                
        }
        this.addEmpresa = function(){
            var sendToWS = $("#add_hardware_form").attr("action");
            var data = {'seccion':'block_empresa',
                          'empresa':$("#singletext").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                        }
            var params = {data:data,url:sendToWS};
            var JX = new Ajaxing(params);
                JX.sendJX();
                
                
        }
        this.addResponsable = function(){
            var sendToWS = $("#add_hardware_form").attr("action");
            var data = {'seccion':'block_responsable_nombre',
                          'responsable':$("#singletext").val(),
                          'empresa':$("#empresa option:selected").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                        }
            var params = {data:data,url:sendToWS};
            var JX = new Ajaxing(params);
                JX.sendJX();
                
                
        }


}

var activ_event_forms = function(){
        this.showforms = function(){
                $(".shower_d").click(function(e){
                        e.preventDefault();        
                        var idchange = $(this).attr("id").replace('to_','');
                        $(".shower_d").removeClass("revas_unem_onn");
                        $(this).addClass("revas_unem_onn");
                        var idchange = $(this).attr("id").replace('to_','');
                        $(".blockes").hide('fast',function(){
                                $("#"+idchange).show('fast');
                                //$("#seccion").val(idchange);
                        });
                        $(".closeded").hide('fast');
                                
                });
                $(".shower").click(function(e){
                        e.preventDefault();
                         var steping = parseInt($(this).attr("href").replace('#',''));
                        var pasos_totales = $(".shower").not(".closeded").length;
                        if(steping>1){
                                antes = steping-1;
                        }
                        var objetivo = $("[href=#"+antes+"]").attr("id").replace('to_','');
                        var validador = true;//validation(objetivo);
                        if(validador){
                                $("#successsadd").text('');
                                var idchange = $(this).attr("id").replace('to_','');
                                $(".shower").removeClass("revas_unem_onn");
                                $(this).addClass("revas_unem_onn");
                                var idchange = $(this).attr("id").replace('to_','');
                                $(".blockes").hide('fast',function(){
                                        $("#"+idchange).show('fast');
                                        //$("#seccion").val(idchange);
                                });
                                $(".closeded").hide('fast');
                                $("#stepp").text(steping);
                                if(steping == pasos_totales){
                                        $("#save").show();
                                }

                                
                        }
                        else{
                                alert("Debe llenar todos los campos");
                        }
                        
                });
                var sendVars = new Summiter;
                $("#save").click(sendVars.singleSrValues);
                $("#ending").click(function(e){
                        e.preventDefault();
                        
                        var pasofinal = $(".shower").not(".closeded").length+1;
                        var paso = $("#stepp").text();
                        var sevale = validation(pasofinal);
                        if (sevale && pasofinal && paso==pasofinal-1) {
                                idd = $("#id_hardware").val();
                                $.ajax({url:'/ending/'+idd+"/",dataType:'json',success:function(response){
                                       //$("#reslut_"+idd).parents(".each_one:first").remove();
                                        alert('Registro finalizado con éxito.');
                                        //$("#view_pending").click();                               
                                       var cerrando = new Lbox();
                                       cerrando.CloseBox();
                                       //var unities = parseInt($("#view_pending").text())-1;
                                       //$("#view_pending").text(unities);
                                       }
                                });
                        }else{
                                alert('La información no esta completa.');
                        }
                });
        }
        
        
        
        this.single_sons_form = function(){
                $(".addsingleton").click(function(e){
                        e.preventDefault();
                        var topd = $(this).attr('offsetTop');
                        var leftd = $(this).attr('offsetLeft');
                        var widhd = $(this).attr('offsetWidth');
                        
                        $("#block_singleton").show('fast').css('top',topd).css('left',leftd+widhd);
                        var type_ax = $(this).attr("id");
                        var sendVars = new Summiter;
                        switch (type_ax) {
                                case "to_marca":
                                        $("#title_cap").text("MARCA");
                                        $("#save_single").unbind().click(function(){
                                                //$("#seccion").val("block_marca");
                                                sendVars.addMarca();
                                        });
                                break;
                                case "to_modelo":
                                        var parent_id = $("#marca").val();
                                        if (parent_id!=0) {
                                                $("#title_cap").text("MODELO");
                                                $("#save_single").unbind().click(function(){
                                                    //$("#seccion").val("block_modelo");
                                                    sendVars.addModelo();
                                                });
                                        }
                                        else{
                                                alert("Debe Seleccionar una MARCA.");
                                        }
                                break;
                                case "to_edificio":
                                        $("#title_cap").text("EDIFICIO");
                                        $("#save_single").unbind().click(function(){
                                                //$("#seccion").val("block_edificio");
                                                sendVars.addEdificio();
                                        });
                                break;
                                case "to_empresa":
                                        $("#title_cap").text("EMPRESA");
                                        $("#save_single").unbind().click(function(){
                                                $("#seccion").val("block_empresa");
                                                sendVars.addEmpresa();
                                        });
                                break;
                                case "to_responsable_nombre":
                                        $("#title_cap").text("RESPONSABLE");
                                        var parent_id = $("#empresa").val();
                                        if(parent_id){
                                        $("#save_single").unbind().click(function(){
                                                //$("#seccion").val("block_responsable_nombre");
                                                sendVars.addResponsable();
                                        });
                                        }
                                        else{
                                                alert("Debe seleccionar una empresa");
                                        }
                                break;
                                
                                        
                        }
                });
        }
        this.load_models = function(default_value){
            var data = {  'marca':$("#marca option:selected ").val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
            }
            $.ajax({url:'modelos',data:data,type:'POST',dataType:'json',success:function(response){
                    $("#modelo").html('');
                    $.each(response,function(x,items){
                        var option_modelo = document.createElement('option');
                            option_modelo.value = items.pk;
                            option_modelo.innerHTML = items.text;
                        $("#modelo").append(option_modelo);
                    });
                    if (default_value) {
                        $("#modelo").val(default_value);
                    }

                }
            });            
        }
        this.load_responsables = function(default_value){
                var data = {empresa:$("#empresa option:selected").val(),
                            'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
                }
                $.ajax({url:'responsables',data:data,type:'POST',dataType:'json',success:function(response){
                        $("#responsable").html('');
                        $.each(response,function(x,items){
                            var option_responsables = document.createElement('option');
                                option_responsables.value = items.pk;
                                option_responsables.innerHTML = items.text;
                            $("#responsable").append(option_responsables);
                        });
                        if(default_value){
                                $("#responsable").val(default_value);
                        }
    
                    }
                });
        }
}

var Lbox = function(){
        this.CloseBox = function(){
                $("#lbox").hide('fast');
                $("#back_lbox").fadeTo('',0,function(){
                        $("#back_lbox").hide();        
                });
        }
}

$.fn.LoadForm=function(data){
    $(this).click(function(e){
        e.preventDefault();
        scrollTo(0,0);
        if(data){
                var victim = $(this).attr("id").replace('reslut_','');
                data = '?id_editando='+victim;
        }
        $("#back_lbox").show().fadeTo('',0.8,function(){
                        $("#lbox").show('fast');
                        var form_ax = new activ_event_forms;
                        $("#lbox_content").html('<div class="cargando">Cargando...</div>').load('/addform'+data,function(){
                                $.getScript('/media/js/ips.js');
                                $("#closer_box").CloseForm();
                                form_ax.showforms();
                                form_ax.single_sons_form();
                                $(".date").datepicker({"dateFormat":"dd/mm/yy"});
                                $("#marca").change(function(){ form_ax.load_models(null)});
                                $("#empresa").change(function(){form_ax.load_responsables(null);});
                                $("#tipo_hdwre").likes_dislikes();
                                $("#close_single").click(function(e){
                                        e.preventDefault();
                                        $("#block_singleton").hide('fast');
                                });
                                $("#formato_nombre").change(function(){
                                    var valor = $(this).val();
                                    if(valor=='ENCLOUSURE' || valor=='POWER ENCLOUSURE'){
                                        $("#unidades").addClass('closeded').parents("div:first").hide();    
                                        $("#no_enclousure").removeClass('closeded').parents("div:first").show();
                                        $("#posicion_enclousure").removeClass('closeded').parents("div:first").show();
                                    }
                                    else{
                                            $("#no_enclousure").addClass('closeded').parents('div:first').hide();
                                            $("#posicion_enclousure").addClass('closeded').parents('div:first').hide();
                                            $("#unidades").removeClass('closeded').parents('div:first').show();


                                        }
                                });
                        });
                });
        
    });
}

$.fn.OpenLbox=function(){
        $(this).click(function(e){
                e.preventDefault();
                var who_are_you =  $(this).attr("href").replace("#","");
                
                scrollTo(0,0);
                if(who_are_you=='log_buton'){
                var victim = $(this).attr("id").replace('log_','');
                $("#back_lbox").show().fadeTo('',0.8,function(){
                        $("#lbox").show('fast');
                        $("#lbox_content").html('<div class="cargando">Cargando...</div>').load("/log/"+victim+"/",function(){
                                
                                
                        });
                });
                }
                else{
                var victim = $(this).attr("id").replace('info_','');
                $("#back_lbox").show().fadeTo('',0.8,function(){
                        $("#lbox").show('fast');
                        $("#lbox_content").html('<div class="cargando">Cargando...</div>').load("/details?id_detail="+victim,function(){
                                var form_ax = new activ_event_forms;
                                form_ax.showforms();
                        });
                });
                }
        });
}

$.fn.CloseForm = function(){
    $(this).click(function(e){
        e.preventDefault();    
        $("#lbox").hide('fast');
        $("#back_lbox").fadeTo('',0,function(){
                $("#back_lbox").hide();
                $("#lbox_content").html('');
        });    });
}


$.fn.EditButon = function(){
        var victim = $(this).attr("id").replace('reslut_','');
        var data = '?id_editando='+victim;
        $(this).LoadForm(data);
}

$.fn.NavPages = function(data){
        $(this).click(function(e){
                e.preventDefault();
                var url_new = $(this).attr("href").replace('#','');
                $("#result_box").html('<div class="cargando">cargando...</div>').load('/searching'+url_new,data,function(){
                    $(".next_page").NavPages(data);
                    $(".edit_buton").LoadForm(this);
                    $(".cargando").remove();
                    $(".detail_buton").OpenLbox();
                    $(".log_buton").OpenLbox();
                    
                });
        });
}
$.fn.searchEngine = function(datus){
        $(this).submit(function(e){
                e.preventDefault();
                var data = datus?datus:$(this).serializeArray();
                $("#result_box").html('<div class="cargando">cargando...</div>').load('/searching',data,function(){
                        $(".next_page").NavPages(data);
                        $(".edit_buton").LoadForm(this);
                        $(".cargando").remove();
                        $(".detail_buton").OpenLbox();
                        $(".log_buton").OpenLbox();
                });
        });
}

        
$.fn.showfilter = function(){
        $(this).toggle(function(e){
                e.preventDefault();
                $("#filter_box").animate({height:'200px'});
                $("#stater_buton").addClass("down_btn");
        },
        function(e){
                e.preventDefault();
                $("#filter_box").animate({height:'30px'});
                $("#stater_buton").removeClass("down_btn");
        });
        
}

$.fn.load_user_page=function(){
        $(this).click(function(e){
                e.preventDefault();
                $("#result_box").load("/users/");
        });
        
}

$(document).ready(function(){
    $("#add_buton").LoadForm('');
    $("#cerrar_lbox").CloseForm();
    $("#search_form").searchEngine('');
    $("#add_hardware_form").submit(function(e){e.preventDefault();});
    $("#view_pending").click(function(e){
        e.preventDefault();
        $("#text_search").val('pendientes');
        $("#search_form").submit();
        
    });
    
    $("#ft_marca").change(function(){
            var default_value=null;
            var data = {  'marca':$(this).val(),
                          'csrfmiddlewaretoken':$("[name=csrfmiddlewaretoken]").val()
            }
            $.ajax({url:'modelos',data:data,type:'POST',dataType:'json',success:function(response){
                    $("#ft_modelo").html('<option value="">Seleccione Modelo</option>');
                    $.each(response,function(x,items){
                        var option_modelo = document.createElement('option');
                            option_modelo.value = items.pk;
                            option_modelo.innerHTML = items.text;
                        $("#ft_modelo").append(option_modelo);
                    });
                    if (default_value) {
                        $("#ft_modelo").val(default_value);
                    }

                }
            });            
        
            
    });
    $("#users_buton").load_user_page();
    $("#rack_manager_btn").click(function(e){
        e.preventDefault();
        $("#result_box").load("/rack_form/");
    });
    
    
        $(".editmyuser").click(function(e){
        e.preventDefault();
        var idu = $(this).attr("id").replace('result_','');
        
        $("#result_box").load("/userform/"+idu+"/",function(){
            $("#form_usuario").senduser();
            $.getScript("/media/js/userax.js");
        }); 
    });
   
    $("#import_ocsweb_btn").click(function(e){
        e.preventDefault();
        $("#result_box").html('<br><br><br><br><br><h4>Se esta importando la información del OCSI, este proceso demorará algun tiempo, por favor espere...</h4>');
        $.ajax({url:'/migrate',dataType:'json',
                success:function(response){
                    $("#result_box").html('<br><br><br><br><h4>Se actualizarón '+ response.cantidad + ' Registros</h4>');
                    
                }
                
                
        });

        
    });


});
