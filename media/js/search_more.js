$(".date_s").datepicker({"dateFormat":"yy-mm-dd"});


function fdate(data){
    var first_date = {name:$("[name=first_date]").val() + $("[name=operator_date]").val(),
                      value:$("[name=dated]").val()};
    var last_date = {name:$("[name=last_date]").val() + $("[name=operator_date_late]").val(),
                      value:$("[name=datedlast]").val()};
    if($("[name=datedlast]").val().length>9 )
        data.push(first_date);
    if($("[name=dated]").val().length>9)
        data.push(last_date);
    return data;
}

$.fn.advancedSearch = function(){
        $(this).toggle(function(e){
                e.preventDefault();
                var hh = $("#moresearch").attr("offsetHeight");
                $("#filter_box").animate({height:parseInt(hh+140)+'px'});
                $("#stater_buton").addClass("down_btn");
        },
        function(e){
                e.preventDefault();
                $("#filter_box").animate({height:'50px'});
                $("#stater_buton").removeClass("down_btn");
        });       
}

$.fn.submiter = function(params_search){
        //var data = params_search;
        var data = $(this).serializeArray();
        //data.push(datas);
        $(this).submit(function(e){
                e.preventDefault();
                names = {};
                var liga = $("#liga_txt").val();
                $(".campo").each(function(k,v){
                    names[k]=$(v).val();
                });
                $(".texto").each(function(k,v){
                    name_srch = names[k];
                    value_srch = $(v).val();
                    data.push({name:name_srch,value:value_srch});
                });
                
                var first_dateq = {name:$("[name=first_date] option:selected").val() + $("[name=operator_date] option:selected").val(),
                      value:$("[name=dated]").val()};
                var last_dateq = {name:$("[name=last_date] option:selected").val() + $("[name=operator_date_late] option:selected").val(),
                      value:$("[name=datedlast]").val()};
                if($("[name=dated]").val().length>9 && $("[name=first_date] option:selected").val().length>2)
                            data.push(first_dateq);
                if($("[name=datedlast]").val().length>9 && $("[name=last_date] option:selected").val().length>2)
                            data.push(last_dateq);
                $("#result_box").html('<div class="cargando">cargando...</div>').load(liga,data,function(){
                        $(".next_page").NavPages(data);
                        $(".edit_buton").LoadForm(this);
                        $(".cargando").remove();
                        $(".detail_buton").OpenLbox();
                });
        });
}

    $("#stater_buton,#advanceds").advancedSearch();
    $("#filter_form").submiter(params_search);


    $(".del_buton").click(function(e){
        e.preventDefault();
        var yooo = $(this);
        var victim = $(this).attr("id").replace("kll_","");
        aceptodel = confirm('Esta seguro que desea eliminar este registro,\n la información sera borrada de la base de datos.');
        if (aceptodel) {
                $(this).parents(".each_one:first").remove();                
                $.ajax({url:'/deleting_h/'+victim+'/',dataType:'json',success:function(response){
                        alert(response.removed);        
                        }
                });
        }
        
        
    });



// AGREGAR CAMPOS PARA BSUCAR TEXTO
$("#mas_txts").click(function(e){
    e.preventDefault();
    var elementos = $("#buscando_txt").clone();
    $(elementos).removeAttr('id');
    $(elementos).find(".texto:first").val('');
    var unlinker = document.createElement('a');
    unlinker.innerHTML=('cancelar');
    unlinker.className='linka';
    $(unlinker).attr('href','#killme');
    $(unlinker).click(function(e){
        e.preventDefault();
        $(unlinker).parents("div:first").remove();
    });
    $(elementos).append(unlinker);
    $("#mas_busqueda").append(elementos);

});


$("#on_screen").click(function(){
    $("#liga_txt").val('/searching');

});

// PARA GENERAR REPORTES ---------------------------------------------------------------
$("#descargar_reporte").click(function(){
    $("#liga_txt").val('/reporter');    
});
//----------------------------------------------------------------------------------------



