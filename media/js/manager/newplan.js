function agregarPlan(response){
        alert(response);
        $("#t_nombre_plan strong").text(response.name);
        $("[name=planid]").val(response.id);
}


function addedFood(response){
        alert("Se agregó con éxito");
        location.reload();
}

function editedFood(response){
        alert("editadocon éxito.");
        location.reload();
}

var AjaxBurn = function(params){
        // params is some like this
        // {url:'some/url/',type:'post/get',data:serializeDataJson}
        this.params = params;
        this.goAJX = function(){
                $.ajax({url:this.params.url,
                       dataType:'json',
                        type:this.params.type,
                        data:this.params.data,                        
                        success:function(response){
                                if(typeof(window[response.sux])==="function"){
                                        window[response.sux](response);
                                        }
                        }
                        
                });
        }

}
var AddPlan = function(){

        this.addPlanName = function(e){
              e.preventDefault();
              var plan_name= $("#plan_name").val();
              var data = $(this).serializeArray();
              var urlGo = $(this).attr("action");
              var typeSEND = $(this).attr("method");   
              var ajx = {url:urlGo,type:typeSEND,data:data,texto:plan_name,sux:agregarPlan};
              var ajaxing = new AjaxBurn(ajx);
              ajaxing.goAJX();

              $("#main_controls,#menu_dia").show();
              $("#listadodrag").show();
              $("#box_nombre_plan").hide();
              $("#box_plan_comida").show();
               $('.dia_box').show();
              

        }


}




var foodPrcess = function(params){
        this.params = params;
        
        
}

        
delElemento = function(){
              var idpix = $(this).attr("id").replace("del_","");
              if(confirm("¿Esta seguro que desea eliminar este elemento?")){
                $(this).parents(".meals_box:first").remove();
                $.ajax({url:'/manager/delplanpice/'+idpix+'/',
                        success:function(response){
                        alert(response);
                        }
                });
              }
        }
// Seccion para agregar tabs / brag&drop / guardar elementos de un Aplan
//
    var Elementor = function(){ 
        this.constructore = function(elemental){
            var elem = document.createElement(elemental.typens);
            elemental.txtr?$(elem).text(elemental.txtr):null;
            $.each(elemental.attrs,function(k,v){
                $(elem).attr(k,v);
            });
            return elem;
        }
    }
    
    function delTab(elementor){
        $(elementor).click(function(e){
            e.preventDefault();
            if($(".kitfoodlist").length>1){
                $(elementor).parents(".kitfoodlist:first").slideUp('speed',function(){
                    $(elementor).parents(".kitfoodlist:first").remove();    
                });
                
           
            }
            else
                alert("debe existir por lo menos un elemento.");
        });
    }
  
  
  function HowAndWhy(params){
        var itemNist = {'typens':'li','name':'contenedor',
                        'txtx':params.textus,
                        'attrs':{'class':'plist from_'+params.id}};

        var itemTxtC = {'typens':'input','name':'texto_cantidad',
                        'attrs':{
                                 'name':'food_'+params.id,
                                 'class':'smalltxt',
                                 'type':'text'
                                 }
                        };

        var hiddentext = {'typens':'input','attrs':{'type':'hidden','name':'name_food_'+params.id,'value':params.textus}}
        var itemTxtex = {'typens':'input',
                        'name':'texto_cantidad',
                        'attrs':{'name':'extra_food_'+params.id,'type':'text'}
                        }
        var itemDel = {'typens':'a',
                        'name':'texto_cantidad',
                        
                        'attrs':{'href':'#deletore','class':'inline_blocke dd'}
                        };

        var itemRemplaza = {'typens':'input','name':'remplaza_',
                            'attrs':{'type':'checkbox','value':'1','class':'astepaya','name':'remplaza_'+params.id}
                           };

        var itemText = {'typens':'div','txtr':params.textus,'attrs':{'class':''}
                       };

        var contenedor = new Elementor();
        var boxi = contenedor.constructore(itemNist);
        var cantidad = contenedor.constructore(itemTxtC);
        var extra = contenedor.constructore(itemTxtex);
        var deletos = contenedor.constructore(itemDel);
        var replazo = contenedor.constructore(itemRemplaza);
        var textusAlimentus = contenedor.constructore(itemText);
        var inputNameFood = contenedor.constructore(hiddentext);
        $(deletos).click(function(e){
            e.preventDefault();
            
            var dellistvalue = $(this).parents("form:last").find(".listado_target").val().replace(','+params.id,"").replace(params.id,"");
            $(this).parents("form:last").find(".listado_target").val(dellistvalue);
            $(this).parents("li:first").remove();
        });
        if($(params.targets).find(".from_"+params.id).length<1){
            $(boxi).appendTo(params.targets);
            $(textusAlimentus).appendTo(boxi);
            $(cantidad).appendTo(boxi);
            $(extra).appendTo(boxi);
            $(replazo).appendTo(boxi);
            $(deletos).append('<img src="/media/image_site/close.png" border="0"/>');
            $(deletos).appendTo(boxi);
            $(inputNameFood).appendTo(boxi);
            var valueMa = $(boxi).parents("form:first").find(".listado_target").val()?","+$(boxi).parents("form:first").find(".listado_target").val():'';
            $(boxi).parents("form:first").find(".listado_target").val(params.id+valueMa);
        }

  }
    
    function neTab(){
        var newtabs = $("#mealkit").clone();
        var nextid = $(".meals_box").length+1;
        $(newtabs).find(".newtab").remove();
        $(newtabs).attr("id","meal"+nextid);
        $(newtabs).find("[name=colation]").val(nextid);
        $(newtabs).find(".itxt").val(" ");
        $(newtabs).find(".deletab").show();
        $(newtabs).find("[name=foodtime]").val("");
        $(newtabs).find("[name=foodtime_list]").val("");
        
        $(newtabs).find(".foodlist").html("<li>&nbsp;</li>").droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function( event, ui ) {
                    $( this ).find( ".placeholder" ).remove();
                    var params = {id:ui.draggable.attr("id"),
                                  textus:ui.draggable.text(),
                                  typeFood:ui.draggable.attr("class"),
                                  targets:this}
                    HowAndWhy(params);
                }
        });
        $(newtabs).find(".saver_plan").click(function(e){
                e.preventDefault();
                var data = $(this).parents("form:first").serializeArray();
                $.ajax({url:'/manager/addPlanParts',data:data,type:'post'}); 
        });
        delTab($(newtabs).find(".deletab"));
        $('.kitfoodlist:last').after(newtabs);
    }
    
    function resetTab(){
        var newtabs = $("#meal1").clone();
        $(newtabs).find(".foodlist").html("<li>Alimento</li>").droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function( event, ui ) {
                    $( this ).find( ".placeholder" ).remove();
                    var params = {id:ui.draggable.attr("id"),
                                  textus:ui.draggable.text(),
                                  typeFood:ui.draggable.attr("class"),
                                  targets:this}
                    HowAndWhy(params);
                }
        });
        $("#new_meal").html(newtabs);
    }

//$(document).ready(function(){
//    var axform = new AddPlan();
//    $("#planname").submit(axform.addPlanName);
//});







