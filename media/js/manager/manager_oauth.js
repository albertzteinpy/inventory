var OauthSys= function(params){
    this.params = params;
    this.loginAx = function(e){
        $(".err").html("Espere un momento...");
        e.preventDefault();
        if($("[name=correo]").val().length > 0 &&  $("[name=clave]").val().length > 0 ){
            var parametres = $(this).serializeArray();
            $.ajax({url:'/manager/login',type:'post',
                   data:parametres,success:function(response){
                        
                        if(response=='inicio'){
                            $(".err").html("Información correcta, cargando información...");
                            window.location='/manager/welcome';
                            
                        }
                        else{
                            $(".err").html("Usuario y/o contraseña incorrectos.");
                        }
                   }
            });
        }
        else{
            $(".err").html("Debe proporcionar sus datos.");    
        }
    }
}


$(document).ready(function(){
    var loginprocess = new OauthSys('x');
    $("#login_form").submit(loginprocess.loginAx);    
});