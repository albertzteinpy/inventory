function deluser(response){
    $(response.forma).hide();
    $(response.forma).next("guardando la información de usuario...");
    var data = $(response.forma).serializeArray();
    $.ajax({url:response.url,
            type:'post',
            dataType:'json',
            data:data,
            success:function(responsejx){
                $("#box").html("información guardada con éxito, espere u nmomento...");
                if(responsejx.echo=="ok"){
                    location.reload();    
                }   
            }
    });
}


var Usermanager = function(params){
        this.params = params;
        this.formuser = function(params){
            $(params.targetview).load(params.url,function(){
                $("#newuserfrm").submit(function(e){
                    e.preventDefault();
                    $(".err").remove();
                    var a = 0;
                    $(".void").each(function(){
                        if($(this).val().length<1){
                            $(this).before('<div class="errform">Debe proporcionar esta información.</div>');
                            a++;
                        }
                    });
                    if(a==0){
                        params.forma = $(this);
                        params.url = "/manager/adduser/";
                        if(typeof(window[params.sux])==="function"){
                            window[params.sux](params);
                        }
                    }
                    
                });                
            });
        }
        
        this.deluser = function(){
            var url = $(this).attr("href").replace("#","");
            var donde = $(this).parents(".plan_list:first").load(url);
        }
        this.edituser = function(){
            var idu = $(this).attr("id").replace("x","");
            $("#box").load("/manager/usereditform/"+idu+"/",function(){
                $(window).scrollTop(0);
                $("#newuserfrm").submit(function(e){
                    e.preventDefault();
                    $(".err").remove();
                    var a = 0;
                    $(".void").each(function(){
                        if($(this).val().length<1){
                            $(this).before('<div class="errform">Debe proporcionar esta información.</div>');
                            a++;
                        }
                    });
                    if(a==0){
                        params = {'forma':$(this),
                                   'url':'/manager/adduser/'};
                        if(typeof(window["deluser"])==="function"){
                            window["deluser"](params);
                        }
                    }
                    
                });                
            });

        }
        this.addgroup = function(e){
            e.preventDefault();
            var data = $(this).serializeArray();
            $.ajax({url:'/manager/managegroup',data:data,
                   dataType:'json',
                   type:'post',success:function(response){
                        if(response.existe==true){
                        $("#grupos").append('<option value="'+response.id+'" >'+response.name+'</option>');
                        $("#grupos").val(response.id);
                        $(".responderg").html("Guardadó con éxito.");

                        }
                        else{
                            $("#grupos").val(response.id);
                            $("#grupos option:selected").text(response.name);
                            $(".responderg").html("Guardadó con éxito.");
                        }
                   }
            });
        }
        this.editg = function(){
            $(".responderg").html("");
            var idg = $(this).find("option:selected").val();
            var texto = $(this).find("option:selected").text();
            $("[name=idgrupo]").val(idg);
            $("[name=grupo]").val(texto);
            $(".bll").show();
            
        }
        this.reseter = function(e){
            e.preventDefault();
            document.getElementById('addgroup').reset();
        }
        this.delg = function(e){
            e.preventDefault();
            $(".responderg").html("Espere por favor...");
            var idg = $("[name=idgrupo]").val();
            if(idg!=0){
                $.ajax({url:'/manager/delg/'+idg+'/',
                       success:function(response){
                            $(".responderg").html(response);
                            document.getElementById('addgroup').reset();
                            $("#grupos").val(idg);
                            $("#grupos option:selected").remove();
                       }
                });
            }
        }
}


$(document).ready(function(){
    var  managementU = new Usermanager();
    var newuser = {'targetview':$("#box"),
                    'url':'/manager/userform',
                    'sux':'deluser'
                    };
    
    $("#newuser").click(function(){
        $("#box").html("Cargando información espere un momento...");
        managementU.formuser(newuser);    
    });
    $(".deluser").click(managementU.deluser);
    $(".edituser").click(managementU.edituser);
    $("#addgroup").submit(managementU.addgroup);
    $("#grupos").change(managementU.editg);
    $("#restart").click(managementU.reseter);
    $("#delg").click(managementU.delg);
});
